EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Voltage Regulator - Switch Mode - 32v in - 3v3 out"
Date "2021-03-06"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5925 3400 6025 3400
$Comp
L power:GNDREF #PWR03
U 1 1 5E83FF10
P 6200 3900
F 0 "#PWR03" H 6200 3650 50  0001 C CNN
F 1 "GNDREF" H 6205 3727 50  0001 C CNN
F 2 "" H 6200 3900 50  0001 C CNN
F 3 "" H 6200 3900 50  0001 C CNN
	1    6200 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR02
U 1 1 5E840EA2
P 4875 3900
F 0 "#PWR02" H 4875 3650 50  0001 C CNN
F 1 "GNDREF" H 4880 3727 50  0001 C CNN
F 2 "" H 4875 3900 50  0001 C CNN
F 3 "" H 4875 3900 50  0001 C CNN
	1    4875 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4875 3800 4875 3900
$Comp
L Device:L_Small L1
U 1 1 5E797EF5
P 5825 3400
F 0 "L1" V 6010 3400 50  0000 C CNN
F 1 "3.9uH" V 5919 3400 50  0000 C CNN
F 2 "Inductor_SMD:L_1812_4532Metric" H 5825 3400 50  0001 C CNN
F 3 "~" H 5825 3400 50  0001 C CNN
F 4 "732-4063-1-ND" V 5825 3400 50  0001 C CNN "Digikey"
	1    5825 3400
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDREF #PWR01
U 1 1 5E84125A
P 4175 3900
F 0 "#PWR01" H 4175 3650 50  0001 C CNN
F 1 "GNDREF" H 4180 3727 50  0001 C CNN
F 2 "" H 4175 3900 50  0001 C CNN
F 3 "" H 4175 3900 50  0001 C CNN
	1    4175 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 601BFCB7
P 4175 3800
F 0 "C1" H 4267 3846 50  0000 L CNN
F 1 "10uF" H 4267 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4175 3800 50  0001 C CNN
F 3 "~" H 4175 3800 50  0001 C CNN
F 4 "1276-6736-1-ND" H 4175 3800 50  0001 C CNN "Digikey"
	1    4175 3800
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:AP63203WU U1
U 1 1 601BDA0D
P 4875 3500
F 0 "U1" H 4875 3867 50  0000 C CNN
F 1 "AP63203WU" H 4875 3776 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-6" H 4875 2600 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP63200-AP63201-AP63203-AP63205.pdf" H 4875 3500 50  0001 C CNN
F 4 "‎AP63203WU-7DICT-ND‎" H 4875 3500 50  0001 C CNN "Digikey"
	1    4875 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4075 3400 4175 3400
Wire Wire Line
	4175 3400 4175 3700
Wire Wire Line
	4175 3400 4350 3400
Connection ~ 4175 3400
Wire Wire Line
	4475 3600 4350 3600
Wire Wire Line
	4350 3600 4350 3400
Connection ~ 4350 3400
Wire Wire Line
	4350 3400 4475 3400
Wire Wire Line
	5275 3400 5625 3400
$Comp
L Device:C_Small C3
U 1 1 601ED102
P 6200 3800
F 0 "C3" H 6292 3846 50  0000 L CNN
F 1 "22uF" H 6292 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6200 3800 50  0001 C CNN
F 3 "~" H 6200 3800 50  0001 C CNN
F 4 "1276-2728-1-ND" H 6200 3800 50  0001 C CNN "Digikey"
	1    6200 3800
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 601F1F8F
P 6400 3800
F 0 "C4" H 6492 3846 50  0000 L CNN
F 1 "22uF" H 6492 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6400 3800 50  0001 C CNN
F 3 "~" H 6400 3800 50  0001 C CNN
	1    6400 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR05
U 1 1 601F92C2
P 6400 3900
F 0 "#PWR05" H 6400 3650 50  0001 C CNN
F 1 "GNDREF" H 6405 3727 50  0001 C CNN
F 2 "" H 6400 3900 50  0001 C CNN
F 3 "" H 6400 3900 50  0001 C CNN
	1    6400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3700 6400 3400
Wire Wire Line
	6200 3400 6200 3700
Wire Wire Line
	5275 3600 6025 3600
Wire Wire Line
	6025 3600 6025 3400
Connection ~ 6025 3400
Connection ~ 6200 3400
Wire Wire Line
	6200 3400 6400 3400
Wire Wire Line
	6025 3400 6200 3400
$Comp
L Device:C_Small C2
U 1 1 602059F7
P 5450 3500
F 0 "C2" V 5675 3450 50  0000 L CNN
F 1 "100n" V 5600 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5450 3500 50  0001 C CNN
F 3 "~" H 5450 3500 50  0001 C CNN
	1    5450 3500
	0    1    -1   0   
$EndComp
Wire Wire Line
	5350 3500 5275 3500
Wire Wire Line
	5550 3500 5625 3500
Wire Wire Line
	5625 3500 5625 3400
Wire Wire Line
	5625 3400 5725 3400
Connection ~ 5625 3400
Connection ~ 6400 3400
Text Notes 4825 2800 0    100  ~ 20
Power Supply
Text HLabel 4075 3400 0    50   Input ~ 0
Vin
Text HLabel 6475 3400 2    50   Input ~ 0
3v3
Wire Wire Line
	6400 3400 6475 3400
$EndSCHEMATC
