/*
**
** This file is part of Heimdall.
**
** Author(s):
**  - Ian Firns <firnsy@gmail.com>
**
** Heimdall is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** BHack Beacon is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Heimdall.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#define STREAMS_DEBUG_ENABLE
#define STREAMS_JSON_ENABLE

//
// define WIFI_SSID, WIFI_PASS, MQTT_USER an MQTT_PASS in wifi_creds.h
// don't forget to make sure it's gitignored too.
#include "wifi_creds.h"

#include <Adafruit_NeoPixel.h>
#include "BHStreams.h"

#define LED       2
#define PIXELS   12
#define RELAY    13

#define LATCH    14
#define STRIKE   15

#define RELAY_DURATION      5000

#define DEBOUNCE_DURATION   3000
#define STATE_PIXELS_COUNT  9

uint8_t statePixels[STATE_PIXELS_COUNT] = {0, 28, 29, 36, 37, 92, 93, 100, 101};

Adafruit_NeoPixel strip = Adafruit_NeoPixel(129, PIXELS, NEO_GRB + NEO_KHZ800);
BHStreams bhs;

enum connectionStates {
  NOT_CONNECTED,
  CONNECTING,
  WIFI_CONNECTED,
  WIFI_WITH_IP,
  WIFI_WITH_MQTT,
  UNKNOWN
};

enum connectionStates connectionState = NOT_CONNECTED;

uint32_t timeLast = 0;
uint32_t statsLast = 0;

uint32_t lastDisplayPixelUpdate = 0;
bool displayActive = true;

bool relayEnabled = false;
uint32_t relayStart = 0;

bool latchOpened = false;
bool latchCurrentState = false;
bool latchSteadyState = false;
bool latchLastState = false;
uint32_t latchLastUpdate = 0;

void setup() {
  Serial.begin(115200);

  pinMode(LED, OUTPUT);
  pinMode(PIXELS, OUTPUT);
  pinMode(RELAY, OUTPUT);

  pinMode(LATCH, INPUT);
  pinMode(STRIKE, INPUT);

  setStatePixels(64, 64, 64);
  setDisplayPixels(64, 0, 64);
  bhs.setNonBlocking(true);

  bhs.begin(WIFI_SSID, WIFI_PASS);
  bhs.setMQTTUsername(MQTT_USER);
  bhs.setMQTTPassword(MQTT_PASS);

  bhs.onStateChange(onStateChange);

  bhs.onTopic("monitor/door", onDoorSolenoid);

  bhs.connect();

  Serial.println("Initialised.");
}

void loop() {
  bhs.process();

  uint32_t now = millis();

  // heartbeat
  if ((now - timeLast) > 500) {
    timeLast = now;
    digitalWrite(LED, !digitalRead(LED));
  }

  // stats update
  if ((now - statsLast) > 300000) {
    statsLast = now;
    writeStats();
  }

  // display pixel timeout

  if (displayActive && (now - lastDisplayPixelUpdate) > 10000) {
    for (uint8_t i=1; i<129; i++) {
      strip.setPixelColor(i, strip.Color(0, 0, 0));
    }

    strip.show();

    render();

    displayActive = false;
  }

  // relay
  if (relayEnabled) {
    if ((now - relayStart) > RELAY_DURATION) {
      bhs.write("monitor/door/solenoid", 0);
      relayEnabled = false;
      setDisplayPixels(64, 0, 0);
      
      digitalWrite(RELAY, LOW);
    }
  }

  // read latch/solenoid
  latchCurrentState = digitalRead(LATCH);

  if (latchCurrentState != latchLastState) {
    if (latchCurrentState) {
      if (!latchSteadyState) {
        latchOpened = false;
        latchSteadyState = true;

        Serial.printf("latch: 0");
        writeLatchState();
      }
      
      latchLastUpdate = now;
    }

    latchLastState = latchCurrentState;
  } else {
    if (latchSteadyState) {
      if ((now - latchLastUpdate) > DEBOUNCE_DURATION) {
        latchSteadyState = false;
        latchOpened = true;

        Serial.printf("latch: 1");
        writeLatchState();
      }
    }
  }
}

void onConnect(void) {
  writeStats();
}

void onStateChange(uint8_t state) {
  if (state == 0) {
    Serial.printf("NOT CONNECTED\n");
    connectionState = NOT_CONNECTED;
  } else if (state == 1) {
    Serial.printf("CONNECTING\n");
    connectionState = CONNECTING;
  } else if (state == 2) {
    Serial.printf("WIFI CONNECTED\n");
    connectionState = WIFI_CONNECTED;
  } else if (state == 3) {
    Serial.printf("WIFI WITH IP\n");
    connectionState = WIFI_WITH_IP;
  } else if (state == 4) {
    Serial.printf("WIFI WITH MQTT\n");
    connectionState = WIFI_WITH_MQTT;
  } else {
    Serial.printf("UNKNOWN STATE: %d\n", state);
    connectionState = UNKNOWN;
  }

  render();
}

void onDoorSolenoid(BHStreamsData *data) {
  bool openDoor = data->toBool();

  if (openDoor) {
    Serial.printf("Turning on\n");
    bhs.write("monitor/door/solenoid", 1);
    relayEnabled = true;
    setDisplayPixels(0, 64, 0);
    digitalWrite(RELAY, HIGH);
    relayStart = millis();
  }
}

void render() {
  if (connectionState == NOT_CONNECTED) {
    setStatePixels(64, 0, 0);
  } else if (connectionState == CONNECTING) {
    setStatePixels(64, 32, 0);
  } else if (connectionState == WIFI_CONNECTED) {
    setStatePixels(0, 0, 64);
  } else if (connectionState == WIFI_WITH_IP) {
    setStatePixels(64, 0, 64);
  } else if (connectionState == WIFI_WITH_MQTT) {
    setStatePixels(0, 64, 0);
  } else {
    setStatePixels(64, 64, 64);
  }
}


void setStatePixels(uint8_t red, uint8_t green, uint8_t blue) {
  for (uint8_t i=0; i<STATE_PIXELS_COUNT; i++) {
    strip.setPixelColor(statePixels[i], strip.Color(red, green, blue));
  }

  strip.show();
}

void setDisplayPixels(uint8_t red, uint8_t green, uint8_t blue) {
  for (uint8_t i=1; i<129; i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }

  strip.show();

  lastDisplayPixelUpdate = millis();
  displayActive = true;
}

void writeLatchState() {
  bhs.write("monitor/door/latch", latchOpened ? "1" : "0");
}

void writeStats() {
  uint32_t now = millis();

  char message[256];
  snprintf(message, 255, "ts=%u ip=%s latch=%s", now, WiFi.localIP().toString(), latchOpened ? "1" : "0");
  bhs.write("monitor/door", message);
}
